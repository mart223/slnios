//
//  ExternalPageViewController.h
//  SLN
//
//  Created by Mart Ernits on 21/08/2020.
//  Copyright © 2020 SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExternalPageViewController : UIViewController <WKNavigationDelegate> {
    
    
    
    
    
}

@property (nonatomic, strong) NSString *pageURL;

@property (weak, nonatomic) IBOutlet WKWebView *webView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actionIndicator;




@end

NS_ASSUME_NONNULL_END
