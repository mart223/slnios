//
//  ViewController.m
//  SLN
//
//  Created by Mart Ernits on 21/08/2020.
//  Copyright © 2020 SL. All rights reserved.
//

#import "ViewController.h"
#import "ExternalPageViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.webView.navigationDelegate = self;
    
    _pageURL = @"https://pure-peak-05978.herokuapp.com";
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [[NSURL alloc] initWithString:_pageURL]];
    [self.webView loadRequest:request];
    
    [self.navigationController setNavigationBarHidden:YES];
    
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    NSString *requestedURL = navigationAction.request.URL.absoluteString;
    
    if (![requestedURL containsString:_pageURL]) {
        
        decisionHandler(WKNavigationActionPolicyCancel);
        
        _externalURL = requestedURL;
        
        [self performSegueWithIdentifier:@"ShowExternalPageSegue" sender:nil];
        
        return;
        
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    if (!webView.loading) {
        [self.actionIndicator stopAnimating];
    }
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ShowExternalPageSegue"]) {
        
        ExternalPageViewController *controller = segue.destinationViewController;
        controller.pageURL = _externalURL;
        
    }
    
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    UIStatusBarStyle result = UIStatusBarStyleLightContent;
    return result;
}


@end
