//
//  ExternalPageViewController.m
//  SLN
//
//  Created by Mart Ernits on 21/08/2020.
//  Copyright © 2020 SL. All rights reserved.
//

#import "ExternalPageViewController.h"

@interface ExternalPageViewController ()

@end

@implementation ExternalPageViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.webView.navigationDelegate = self;
    
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [[NSURL alloc] initWithString:self.pageURL]];
    [self.webView loadRequest:request];
    
    [self.actionIndicator startAnimating];
    
}


- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    if (!webView.loading) {
        [self.actionIndicator stopAnimating];
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
