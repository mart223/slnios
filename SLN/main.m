//
//  main.m
//  SLN
//
//  Created by Mart Ernits on 21/08/2020.
//  Copyright © 2020 SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
