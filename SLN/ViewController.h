//
//  ViewController.h
//  SLN
//
//  Created by Mart Ernits on 21/08/2020.
//  Copyright © 2020 SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>


@interface ViewController : UIViewController <WKNavigationDelegate> {
    
    NSString *_pageURL;
    
    NSString *_externalURL;
    
}

@property (weak, nonatomic) IBOutlet WKWebView *webView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actionIndicator;




@end

