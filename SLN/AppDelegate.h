//
//  AppDelegate.h
//  SLN
//
//  Created by Mart Ernits on 21/08/2020.
//  Copyright © 2020 SL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

